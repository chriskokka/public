namespace HelmesDotNet.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PriorityInt : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Ticket", "Priority", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Ticket", "Priority", c => c.Decimal(precision: 18, scale: 2));
        }
    }
}
