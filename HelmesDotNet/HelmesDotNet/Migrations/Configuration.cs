namespace HelmesDotNet.Migrations
{
using HelmesDotNet.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<HelmesDotNet.DAL.ErrorTrackerContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(HelmesDotNet.DAL.ErrorTrackerContext context)
        {

            var persons = new List<Person>
            {
                new Person { FirstName = "Chris",   LastName = "Kokka"},
                new Person { FirstName = "Ott", LastName = "Sepp"},
                new Person { FirstName = "M�rt",   LastName = "Avandi"},
                new Person { FirstName = "Gert",    LastName = "Kuusk"},
                new Person { FirstName = "Ying",      LastName = "Yang"}
            };
            persons.ForEach(s => context.Persons.AddOrUpdate(p => p.LastName, s));
            context.SaveChanges();

            var tickets = new List<Ticket>
            {
                new Ticket { 
                    Name = "First category.",
                    Date = DateTime.Parse("2014-1-23"),
                    PersonID = persons.Single(s => s.LastName == "Kokka").PersonID,
                    DueDate =  DateTime.Parse("2014-2-24"),
                    Priority = 1,
                },
                new Ticket { 
                    Name = "First category also.",
                    Date = DateTime.Parse("2014-2-20"),
                    PersonID = persons.Single(s => s.LastName == "Avandi").PersonID,
                    DueDate =  DateTime.Parse("2014-2-25"),
                    Priority = 1,
                },
                new Ticket { 
                    Name = "Second category.",
                    Date = DateTime.Parse("2014-1-23"),
                    PersonID = persons.Single(s => s.LastName == "Kokka").PersonID,
                    DueDate =  DateTime.Parse("2014-2-23"),
                    Priority = 2,
                },
                new Ticket { 
                    Name = "Second category also.",
                    Date = DateTime.Parse("2014-2-23"),
                    PersonID = persons.Single(s => s.LastName == "Sepp").PersonID,
                    DueDate =  DateTime.Parse("2014-2-24"),
                    Priority = 2,
                },
                new Ticket { 
                    Name = "Third category.",
                    Date = DateTime.Parse("2014-2-23"),
                    PersonID = persons.Single(s => s.LastName == "Sepp").PersonID,
                    DueDate =  DateTime.Parse("2014-2-27"),
                    Priority = 3,
                },
                new Ticket { 
                    Name = "Third category even more.",
                    Date = DateTime.Parse("2014-2-23"),
                    PersonID = persons.Single(s => s.LastName == "Sepp").PersonID,
                    DueDate =  DateTime.Parse("2014-2-26"),
                    Priority = 3,
                },
                new Ticket { 
                    Name = "Thrid category also.",
                    Date = DateTime.Parse("2014-1-24"),
                    PersonID = persons.Single(s => s.LastName == "Avandi").PersonID,
                    DueDate =  DateTime.Parse("2014-2-28"),
                    Priority = 4,
                },
                new Ticket { 
                    Name = "Thrid category priority 4.",
                    Date = DateTime.Parse("2014-1-24"),
                    PersonID = persons.Single(s => s.LastName == "Avandi").PersonID,
                    DueDate =  DateTime.Parse("2014-2-27"),
                    Priority = 4,
                },
                new Ticket { 
                    Name = "Fourth category.",
                    Date = DateTime.Parse("2014-2-12"),
                    PersonID = persons.Single(s => s.LastName == "Kuusk").PersonID,
                    DueDate =  DateTime.Parse("2014-2-28"),
                    Priority = 5,
                },
                new Ticket { 
                    Name = "Fourth category also.",
                    Date = DateTime.Parse("2014-1-12"),
                    PersonID = persons.Single(s => s.LastName == "Yang").PersonID,
                    DueDate =  DateTime.Parse("2014-2-28"),
                    Priority = 5,
                },
            };

            foreach (Ticket t in tickets)
            {
                    context.Tickets.AddOrUpdate(s => s.Name, t);                 
            }
            context.SaveChanges();
        }
      
    }
}

