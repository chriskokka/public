namespace HelmesDotNet.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Person",
                c => new
                    {
                        PersonID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                    })
                .PrimaryKey(t => t.PersonID);
            
            CreateTable(
                "dbo.Ticket",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Date = c.DateTime(nullable: false),
                        PersonID = c.Int(nullable: false),
                        DueDate = c.DateTime(nullable: false),
                        Priority = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Person", t => t.PersonID)
                .Index(t => t.PersonID);
            
            CreateTable(
                "dbo.Comment",
                c => new
                    {
                        CommentID = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        TicketID = c.Int(nullable: false),
                        PersonID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CommentID)
                .ForeignKey("dbo.Ticket", t => t.TicketID)
                .ForeignKey("dbo.Person", t => t.PersonID)
                .Index(t => t.TicketID)
                .Index(t => t.PersonID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Comment", new[] { "PersonID" });
            DropIndex("dbo.Comment", new[] { "TicketID" });
            DropIndex("dbo.Ticket", new[] { "PersonID" });
            DropForeignKey("dbo.Comment", "PersonID", "dbo.Person");
            DropForeignKey("dbo.Comment", "TicketID", "dbo.Ticket");
            DropForeignKey("dbo.Ticket", "PersonID", "dbo.Person");
            DropTable("dbo.Comment");
            DropTable("dbo.Ticket");
            DropTable("dbo.Person");
        }
    }
}
