namespace HelmesDotNet.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TicketIsCritical : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ticket", "isCritical", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ticket", "isCritical");
        }
    }
}
