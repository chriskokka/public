namespace HelmesDotNet.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NullableComment : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Comment", "TicketID", c => c.Int());
            AlterColumn("dbo.Comment", "PersonID", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Comment", "PersonID", c => c.Int(nullable: false));
            AlterColumn("dbo.Comment", "TicketID", c => c.Int(nullable: false));
        }
    }
}
