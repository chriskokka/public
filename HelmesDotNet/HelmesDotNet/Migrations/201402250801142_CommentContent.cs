namespace HelmesDotNet.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CommentContent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Comment", "Content", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Comment", "Content");
        }
    }
}
