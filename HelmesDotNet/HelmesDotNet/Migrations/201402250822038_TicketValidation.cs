namespace HelmesDotNet.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TicketValidation : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Ticket", "Date", c => c.DateTime());
            AlterColumn("dbo.Ticket", "PersonID", c => c.Int());
            AlterColumn("dbo.Ticket", "DueDate", c => c.DateTime());
            AlterColumn("dbo.Ticket", "Priority", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Ticket", "Priority", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Ticket", "DueDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Ticket", "PersonID", c => c.Int(nullable: false));
            AlterColumn("dbo.Ticket", "Date", c => c.DateTime(nullable: false));
        }
    }
}
