﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HelmesDotNet.Models;
using HelmesDotNet.DAL;

namespace HelmesDotNet.Controllers
{
    public class TicketController : Controller
    {
        private ErrorTrackerContext db = new ErrorTrackerContext();

        //
        // GET: /Ticket/
        public ActionResult Index(int? searchID = null)
        {
            if (searchID != null)
            {
                return RedirectToAction("Edit", new { id = searchID });
            }

            var tickets = from t in db.Tickets
                          select t;

            List<Ticket> ticketList = tickets.ToList();
            List<Ticket> newTicketList = new List<Ticket>();
            List<Ticket> priorityOneList = new List<Ticket>();
            List<Ticket> priorityTwoList = new List<Ticket>();
            List<Ticket> priorityThreeList = new List<Ticket>();
            List<Ticket> priorityRestList = new List<Ticket>();
            int criticalCount = 0;

            foreach (Ticket t in ticketList)
            {
                if (t.Date == null || t.DueDate == null) 
                {
                    priorityRestList.Add(t);
                    continue;
                }

                TimeSpan dateDiff = DateTime.Now - (DateTime)t.DueDate;
                TimeSpan errorLength = (DateTime)t.DueDate - (DateTime)t.Date;

                if ((dateDiff.TotalDays >= 1) && (t.Priority == 1))
                {
                    priorityOneList.Add(t);
                    continue;
                }
                else if ((dateDiff.TotalDays >= 2) && (t.Priority == 2)) 
                {
                    priorityTwoList.Add(t);
                    continue;
                }
                else if (errorLength.TotalDays < 5)
                {
                    priorityThreeList.Add(t);
                    continue;
                }
                else
                {
                    priorityRestList.Add(t);
                    continue;
                }
            }

            priorityOneList = priorityOneList.OrderBy(s => s.DueDate).ToList();
            foreach (Ticket t1 in priorityOneList)
            {
                if (criticalCount < 3)
                {
                    t1.isCritical = true;
                    criticalCount++;
                    newTicketList.Add(t1);
                }
                else
                {
                    newTicketList.Add(t1);
                }
            }
            priorityTwoList = priorityTwoList.OrderBy(s => s.DueDate).ToList();
            foreach (Ticket t2 in priorityTwoList)
            {
                if (criticalCount < 3)
                {
                    t2.isCritical = true;
                    criticalCount++;
                    newTicketList.Add(t2);
                }
                else
                {
                    newTicketList.Add(t2);
                }
            }
            priorityThreeList = priorityThreeList.OrderBy(s => s.DueDate).ToList();
            foreach (Ticket t3 in priorityThreeList)
            {
                if (criticalCount < 3)
                {
                    t3.isCritical = true;
                    criticalCount++;
                    newTicketList.Add(t3);
                }
                else
                {
                    newTicketList.Add(t3);
                }
            }
            priorityRestList = priorityRestList.OrderBy(s => s.DueDate).ToList();
            foreach (Ticket t4 in priorityRestList)
            {
                if (criticalCount < 3)
                {
                    t4.isCritical = true;
                    criticalCount++;
                    newTicketList.Add(t4);
                }
                else
                {
                    newTicketList.Add(t4);
                }
            }


            return View(newTicketList);
        }

        //
        // GET: /Ticket/Details/5
        public ActionResult Details(int id = 0)
        {
            Ticket ticket = db.Tickets.Find(id);

            ticket.Comments = ticket.Comments.OrderBy(s => s.Date).ToList();

            var people = from p in db.Persons
                         select p.FirstName;

            ViewBag.personsList = new SelectList(people.Distinct().ToList());

            if (ticket == null)
            {
                return HttpNotFound();
            }
            return View(ticket);
        }


        //
        // POST: /Ticket/Details/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Details(string Content, int id, string personsList)
        {
            string person = personsList;
            Comment comment = new Comment();
            comment.Content = Content;
            comment.Date = DateTime.Now;
            comment.TicketID = id;
            
            var people = from p in db.Persons
                         select p;

            ViewBag.personsList = new SelectList(people.Distinct().ToList());

            var PeopleList = new List<Person>();
            PeopleList = people.Distinct().ToList();

            foreach (Person p in PeopleList) {
                if (p.FirstName == person)
                {
                    comment.PersonID = p.PersonID;
                    break;
                } 
            }

            if (ModelState.IsValid)
            {
                db.Comments.Add(comment);
                db.SaveChanges();
                return RedirectToAction("Details", new {id = id });
            }

            return RedirectToAction("Index");
        }





        //
        // GET: /Ticket/Create
        public ActionResult Create()
        {
            var people = from p in db.Persons
                         select p.FirstName;           

            ViewBag.personsList = new SelectList(people.Distinct().ToList());

            return View();
        }

        //
        // POST: /Ticket/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name, Date, PersonID, DueDate, Priority")] Ticket ticket, string personsList)
        {
            string person = personsList;
            var people = from p in db.Persons
                         select p;
            var PeopleList = new List<Person>();
            PeopleList = people.Distinct().ToList();

            foreach (Person p in PeopleList) {
                if (p.FirstName == person)
                {
                    ticket.PersonID = p.PersonID;
                    break;
                } 
            }
          
            if (ModelState.IsValid)
            {
                db.Tickets.Add(ticket);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
          
            return View(ticket);
        }

        //
        // GET: /Ticket/Edit/5
        public ActionResult Edit(int id = 0)
        {
            Ticket ticket = db.Tickets.Find(id);

            var people = from p in db.Persons
                         select p.FirstName;

            ViewBag.personsList = new SelectList(people.Distinct().ToList(), ticket.Person.FirstName);

            if (ticket == null)
            {
                return HttpNotFound();
            }
            return View(ticket);
        }

        //
        // POST: /Ticket/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( Ticket ticket, string personsList)
        {
            string person = personsList;
            var people = from p in db.Persons
                         select p;
            var PeopleList = new List<Person>();
            PeopleList = people.Distinct().ToList();

            foreach (Person p in PeopleList)
            {
                if (p.FirstName == person)
                {
                    ticket.PersonID = p.PersonID;
                    break;
                }
            }

            if (ModelState.IsValid)
            {
                db.Entry(ticket).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(ticket);
        }

        //
        // GET: /Ticket/Delete/5
        public ActionResult Delete(int id = 0)
        {
            Ticket ticket = db.Tickets.Find(id);
            if (ticket == null)
            {
                return HttpNotFound();
            }
            return View(ticket);
        }

        //
        // POST: /Ticket/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ticket ticket = db.Tickets.Find(id);

            // Remove all the comments attached to the Ticket.
            ICollection<Comment> commentList = ticket.Comments;
            for (int i = 0; i < commentList.Count; i++ )
            {
                db.Comments.Remove(commentList.ElementAt(i));
                db.SaveChanges();
            }
            db.Tickets.Remove(ticket);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}