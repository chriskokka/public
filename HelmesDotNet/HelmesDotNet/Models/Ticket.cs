﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HelmesDotNet.Models
{
    public class Ticket
    {
        public Ticket()
        {
            isCritical = false;
        }

        public int ID { get; set; }

        [StringLength(100, MinimumLength = 3)]
        [Required]
        public string Name { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? Date { get; set; }

        public virtual int? PersonID { get; set; }      

        [Display(Name = "Due Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DueDate { get; set; }

        [Range(1, 5)]
        public int? Priority { get; set; }

        public bool isCritical { get; set; }

        public virtual Person Person { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}