﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HelmesDotNet.Models
{
    public class Comment
    {
        public int CommentID { get; set; }

        [Display(Name = "Comment")]
        [StringLength(100, MinimumLength = 3)]
        [Required]
        public String Content { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        public int? TicketID { get; set; }

        public int? PersonID { get; set; }

        public virtual Ticket Ticket { get; set; }
        public virtual Person Person { get; set; }
    }

}