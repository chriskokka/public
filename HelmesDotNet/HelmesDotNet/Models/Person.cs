﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HelmesDotNet.Models
{
    public class Person
    {
        public int PersonID { get; set; }

        [Display(Name = "First Name")]
        public String FirstName{ get; set; }

        [Display(Name = "Last Name")]
        public String LastName { get; set; }

        [Display(Name = "Full Name")]
        public string FullName
        {
            get { return LastName + ", " + FirstName; }
        }

        public virtual ICollection<Ticket> Tickets { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }

}