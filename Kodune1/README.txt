Esimene kodune �lesanne kursusel Objektorienteeritud programmeerimine Java p�hjal.

�lesandeks oli luua klassid selleks, et oleks v�imalik luua Tooteid, mille p�hjal
luua Tellimusi, mille saaks saata Stocki (lattu), v�i neid sealt v�lja saata. Samuti
oli vaja luua Cart, et kasutajad saaksid t�ita ostukorvi. Stocki sees, on realiseeritud
ka inventuuri j�lgimine, milleks on kasutatud klassi Transaction.

