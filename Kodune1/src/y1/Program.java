/*
 * Runner
 *
 * Version 0.1
 *
 * Date 08/02/14
 *
 * Copyright Chris Kokka
 */

package y1;

import y1.pos.Cart;
import y1.pos.Order;
import y1.wms.Item;
import y1.wms.Stock;

import java.util.ArrayList;

/**
 * Test class esimese kodutöö jaoks.
 * @version 0.1 08/02/14
 * @author Chris Kokka
 */
public class Program {

    /**
     * Main funktsioon testimiseks.
     * @param args Unused.
     */
    public static void main (String[] args) {
        Item beer = new Item("Rock", 0.50);
        Item vodka = new Item("Absolut", 7.50);
        Item cider = new Item("Sommersby", 1.20);

        Item tempItem = new Item("Temp", -0.22);
        System.out.println("Adding negative price to item:" + tempItem.toString());
        System.out.println();

        /* Hashcode, equals and Item.toString() testing */
        Item beer2 = new Item("Rock", 0.49);
        System.out.println("Hashcode of: " + beer.toString() + " = " + beer.hashCode());
        System.out.println("Hashcode of: " + beer2.toString() + " = " + beer2.hashCode());
        System.out.println("Hascodes equal: " + (beer.hashCode() == beer2.hashCode()));
        System.out.println("Hashcode of: " + beer.toString() + " = " + beer.hashCode());
        System.out.println("Hashcode of: " + beer.toString() + " = " + beer.hashCode());
        System.out.println("Hascodes equal: " + (beer.hashCode() == beer.hashCode()));
        System.out.println(beer.toString() + " = " + beer2.toString() + " result: " + beer.equals(beer2));
        System.out.println(beer.toString() + " = " + beer.toString() + " result: " + beer.equals(beer));

        /* Receipt no. 1 */
        Order receipt = new Order();
        receipt.add(beer, 4);
        receipt.add(vodka, 5);
        receipt.add(cider, 6);

        System.out.println();
        System.out.println("Adding negative quantity to order:" + receipt.add(beer, -2));
        System.out.println();

        System.out.println("First Order:");
        System.out.println(receipt.toString());
        System.out.println("Adding this to Stock:");
        Stock s = new Stock();
        s.receive(receipt);

        System.out.println("Beer available:");
        System.out.println(s.getAvailable(beer));

        System.out.println("Stock the first time:");
        System.out.println(s.toString());

        System.out.println("Shipping out 3 of each:");
        Order o = new Order();
        o.add(beer, 3);
        o.add(vodka, 3);
        o.add(cider, 3);
        System.out.println("Second Order:");
        System.out.println(o.toString());
        System.out.println();

        s.dispatch(o);
        System.out.println("Stock after shipping:");
        System.out.println(s.toString());

        System.out.println("Beer available:");
        System.out.println(s.getAvailable(beer));


        Order of = new Order();
        of.add(beer, 2);
        s.receive(of);
        System.out.println();
        System.out.println("Adding 2 more beers:");
        System.out.println(of.toString());

        System.out.println("------------------------------------------------");
        System.out.println("ALL TRANSACTIONS: ");
        ArrayList<Stock.Transaction> temp = s.getTransactions();
        for (Stock.Transaction t : temp) {
            t.toString();
            System.out.println();
        }

        System.out.println("------------------------------------------------");
        System.out.println("BEER TRANSACTIONS: ");
        int orderId = -9999;
        ArrayList<Stock.Transaction> beerTransactions = s.getItemTransactions(beer);
        for (Stock.Transaction t : beerTransactions) {
            t.toString();
            System.out.println("Item eraldi: " + t.getItem().toString());
            System.out.println("Order id eraldi: " + t.getOrderId());
            System.out.println("Kogus eraldi: " + t.getQuantity());
            orderId = t.getOrderId();
            System.out.println();
        }
        System.out.println("Viimase Transactioni orderID järgi võetud getOrderiga Order: ");
        System.out.println(Order.getOrder(orderId).toString());
        System.out.println("------------------------------------------------");

        System.out.println("Proovin lisada -1 õlut:");
        Order o2 = new Order();
        System.out.println(o2.add(beer, -1));
        System.out.println("Order: " + o2.toString());

        System.out.println();
        System.out.println(s.toString());

        System.out.println("Proovin saata välja 3 õlut: (Stociks ainult 2)");
        Order o3 = new Order();
        o3.add(beer, 4);
        System.out.println("Result:" + s.dispatch(o3));
        System.out.println("Order: " + o3.toString());


        /* CART TESTING */
        System.out.println("Start tesinting the Cart class: \n");
        Cart cart = new Cart();
        cart.add(beer);
        cart.add(vodka, 2);
        cart.add(cider, 3);

        System.out.println();
        System.out.println("Adding negative quantity to cart:"  + cart.add(cider, -3));
        System.out.println();

        System.out.println(cart.toString());
        System.out.println("TOTAL:" + cart.getTotal() + "\n");

        System.out.println("********************************************");
        System.out.println(cart.add(cider, -3));
        System.out.println("********************************************");

        System.out.println("Võtan ikka 3 õllet ja 3 viina: ");
        cart.change(beer, 3);
        cart.change(vodka, 3);
        System.out.println(cart.toString());
        System.out.println("TOTAL:" + cart.getTotal() + "\n");

        System.out.println("Ei taha siidrit: ");
        cart.remove(cider);
        System.out.println(cart.toString());
        System.out.println("TOTAL:" + cart.getTotal() + "\n");

        System.out.println("Õllet on vähe, 21 on veel vaja: ");
        cart.add(beer, 21);
        System.out.println(cart.toString());
        System.out.println("TOTAL:" + cart.getTotal() + "\n");

        System.out.println("CHECKOUT: ");
        Order co = cart.checkOut();
        System.out.println(co.toString());

        System.out.println("LISAN ORDERI STOCKI: ");
        Stock sC = new Stock();
        sC.receive(co);
        System.out.println("STOCK:");
        System.out.println(sC.toString());

        System.out.println("DISPATCHIN ORDERI STOCKIST: ");
        Order newCo = new Order(co);
        sC.dispatch(newCo);
        System.out.println("STOCK:");
        System.out.println(sC.toString());


        System.out.println(cart.clear());
        System.out.println(cart.toString());
        System.out.println("TOTAL:" + cart.getTotal() + "\n");

        Order osss = new Order();
        Item i = new Item("Absinth", 100.00);
        osss.add(i, 2);
        Stock asd = new Stock();
        asd.receive(osss);
        System.out.println("Stock praegu: " + asd.toString() );
        System.out.println("Proovin lisada teist korda sama Orderi: " + asd.receive(osss));
        System.out.println("Order ise: " + osss.toString());

        /* Receipt no. 2
        Item whiskey = new Item("The Famous Grouse", 17.0);
        Item burboun = new Item("Jack Daniels", 15.00);
        Order receipt2 = new Order();
        receipt2.add(whiskey, 6);
        receipt2.add(burboun, 8);
        s.receive(receipt2);


         Receipt no. 3
        Item scotch = new Item("Black Label", 19.0);
        Order receipt3 = new Order();
        receipt3.add(scotch, 14);
        s.receive(receipt3);

        Order o = new Order();
        o.add(beer, 4);
        o.add(vodka, 2);
        s.dispatch(o);
        System.out.println(s.toString());
         */
    }

}

