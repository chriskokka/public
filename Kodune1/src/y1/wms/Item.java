/*
 * Item
 *
 * Version 0.1
 *
 * Date 08/02/14
 *
 * Copyright Chris Kokka
 */

package y1.wms;

/**
 * Klass esemete / Itemite jaoks.
 * Itemi nimi salvestatakse Stringina.
 * Itemi hind salvestatakse double-ina.
 * @version 0.1 08/02/14
 * @author Chris Kokka
 */
public class Item {

    /**
     * Toote nimi.
     */
    private String name;

    /**
     * Toote hind.
     */
    private double price;

    /**
     * Toote konstruktor.
     * @param name Toote nimi.
     * @param price Toote hind.
     */
    public Item(String name, double price) {
        this.name = name;
        if (price < 0) {
            this.price = 0.00;
        }
        else {
            this.price = price;
        }
    }

    /**
     * Tagastab Toote nime.
     * @return Toote nimi.
     */
    public String getName() {
        return name;
    }

    /**
     * Tagastab Toote hinna.
     * @return Toote hind.
     */
    public double getPrice() {
        return price;
    }

    /**
     * Tagastab Toote stringi kujul.
     * @return Toode stringi kujul.
     */
    @Override
    public String toString() {
        return name + " " + String.format("%.2f", price) + " EUR";
    }

    /**
     * Kontrollib kahe Toote võrdsust.
     * @param o Võrreldav Toode.
     * @return Toodete võrdsus.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        return ((Double.compare(item.price, price) == 0) && name.equals(item.name));
    }

    /**
     * Tagastab Toote hashcode-i.
     * @return Toote hascode.
     */
    @Override
    public int hashCode() {
        int result;
        long temp;
        result = name.hashCode();
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}