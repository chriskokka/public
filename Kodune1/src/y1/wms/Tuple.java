/*
 * Tuple
 *
 * Version 0.1
 *
 * Date 08/02/14
 *
 * Copyright Chris Kokka
 */
package y1.wms;

/**
 * Klass Itemi ja tema koguse hoidmiseks.
 * Item salvestatakse custom Item tüübina.
 * Quantity salvestatakse int-ina.
 * @version 0.1 08/02/2014
 * @author Chris Kokka
 */
public class Tuple {

    /**
     * Hoitav toode.
     */
    private Item item;

    /**
     * Hoitav kogus.
     */
    private int quanitity;

    /**
     * Tuple klassi konstruktor.
     * @param item Hoitav Toode.
     * @param quanitity Hoitav kogus.
     */
    public Tuple(Item item, int quanitity) {
        this.item = item;
        this.quanitity = quanitity;
    }

    /**
     * Tagastab hoitava Toote.
     * @return Toode.
     */
    public Item getItem() {
        return item;
    }

    /**
     * Tagastab hoitava kvantiteedi.
     * @return
     */
    public int getQty() {
        return quanitity;
    }

    /**
     * Muudab hoitavat Toodet.
     * @param item Uus Toode.
     */
    protected void setItem(Item item) {
        this.item = item;
    }

    /**
     * Muudab hoitavat kvantiteeti.
     * @param quanitity
     */
    protected void setQuanitity(int quanitity) {
        this.quanitity = quanitity;
    }

    /**
     * Tagastab Tuple instance-i stringi kujul.
     * @return Tuple stringi kujul.
     */
    @Override
    public String toString(){
        return item.toString() + " Kogus: " + quanitity;
    }
}
