/*
 * Stock
 *
 * Version 0.1
 *
 * Date 08/02/14
 *
 * Copyright Chris Kokka
 */
package y1.wms;

import y1.pos.Order;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Klass Toodete koguste jälgimiseks.
 * Klass hoiab endas erinevaid Tooteid ja nende koguseid.
 * Samuti salvestatakse kõik tegevused erinevate Itemitega.
 * Ühte tehingut Itemiga nimetatakse Transactioniks.
 * Transactionid on saadaval meetoditega: getTransactions() ja getItemTransactions(Item i).
 * @version 0.1 08/02/2014
 * @author Chris Kokka
 */
public class Stock extends Order {

    /**
     * List Stockis hoitavatest Toodetest ja nende kogustest.
     */
    protected ArrayList<Tuple> stockList;

    /**
     * Kõik selles Stockis tehtud Transactionid.
     */
    private ArrayList<Transaction> transactions = new ArrayList<Transaction>();

    /**
     * Klassi Stock konstruktor.
     */
    public Stock() {
        this.stockList = new ArrayList<Tuple>();
    }

    /**
     * Lisab Stocki Orderi.
     * @param order Stocki pandav / saadetav Tellimus.
     */
    public boolean receive(Order order) {
       return process(order, "receive");
    }

    /**
     * Tagastab antud Toote koguse Stockis.
     * @return Toote kogus Stockis.
     */
    public int getAvailable(Item item) {
        for (Tuple t : stockList) {
            if (t.getItem().equals(item)) return t.getQty();
        }
        return 0;
    }

    /**
     * Saadab välja / muudab Toote kogust Stockis.
     * @param order Tellimus, mis välja saata Stockist.
     */
    public boolean dispatch(Order order) {
       return  process(order, "ship");
    }

    /**
     * Stocki sisene funktsioon töötlemaks sisse saadetud Tellimust.
     * Kui on tegemist Stocki saatmisega, siis on type = "receive", kui
     * välja, siis type = "ship", klassi Cart jaoks on lisatud ka
     * type = "change" ja type="remove". Funktsioon tagastab false, kui
     * Stockist tahetakse mingit toodet saata välja rohkem, kui seda on.
     * true , kui töötlemine õnnestub.
     * @param order Töödeldav Tellimus.
     * @param type Tellimuse tüüp: "ship", "receive", "change" või "remove".
     * @return Kas tellimuse töötlemine õnnestus või mitte.
     */
    protected boolean process (Order order, String type) {

        /* Kui Order on juba processed */
        if (getProcessed(order)) return false;
        Iterator<Tuple> iter = order.iterator();
        boolean itemProcessed;

        /* Niikaua, kuni on Tellimuses ridu. */
        while (iter.hasNext()) {
            itemProcessed = false;
            Tuple orderedT = iter.next();
            Item orderedItem = orderedT.getItem();
            int orderedQty = orderedT.getQty();

            /* Iga Toote kohta, mis juba Stockis on. */
            for (int i = 0, n = stockList.size(); i < n; i++) {
                Tuple stockT = stockList.get(i);
                if (stockT.getItem().equals(orderedItem)) {
                    int newQty = -9999;
                    if (type.equals("ship")) {
                        newQty = stockT.getQty() - orderedQty;
                        if (newQty < 0) {
                            return false;
                        }
                        itemProcessed = true;
                        Transaction t = new Transaction(orderedItem, ((-1) * orderedQty), order.getId());
                        transactions.add(t);
                    }
                    else if (type.equals("receive")) {
                        newQty = stockT.getQty() + orderedQty;
                        Transaction t = new Transaction(orderedItem, orderedQty, order.getId());
                        transactions.add(t);
                    }
                    // Klassi Cart jaoks, et muuta toote kvatiteeti.
                    else if (type.equals("change")){
                        newQty = orderedQty;
                        if (newQty < 0) {
                            return false;
                        }
                    }
                    // Klassi Cart jaoks, et eemaldada tooteid.
                    if (type.equals("remove") || newQty == 0) {
                        for (int j = 0, m = stockList.size(); j < m; j++) {
                            Tuple tuple = stockList.get(j);
                            if (tuple.getItem().equals(orderedItem)) {
                                stockList.remove(j);
                                m = stockList.size();
                            }
                        }
                    }
                    else {
                        stockT.setQuanitity(newQty);
                        itemProcessed = true;
                    }

                    /* Leidsime Toote, break */
                    break;
                }
            }

            /* Kui seda Toodet ei olnud juba Stockis, lisa see kui uus Toode. */
            if (!itemProcessed && type.equals("receive")) {
                Transaction t = new Transaction(orderedItem, orderedQty, order.getId());
                transactions.add(t);
                stockList.add(orderedT);
            }
            /* Kui üritatakse välja saata Toodet, mida polnud Stockis. */
            else if (!itemProcessed && type.equals("ship")) {
                return false;
            }

        }

        /* Märgime ära, et Order on processed */
        setProcessed(order);

        return true;
    }

    /**
     * Klass, mis säilitab kõik tegevused Stockis orderitega.
     */
    public class Transaction {

        /**
         * Toode, mille kohta Transaction käib.
         */
        private Item item;

        /**
         * Itemi kogus, millega Transaction tehti.
         */
        private int quantity;

        /**
         * Orderi id.
         */
        private int orderId;

        /**
         * Transaction classi konstruktor.
         * @param item Item, millega Transaction tehti.
         * @param quantity Kogus.
         * @param orderId Orderi id.
         */
        private Transaction(Item item, int quantity, int orderId) {
            this.item = item;
            this.quantity = quantity;
            this.orderId = orderId;
        }

        /**
         * Tagastab Transactioni Orderi id.
         * @return  Transaction Orderi id.
         */
        public int getOrderId() {
            return this.orderId;
        }

        /**
         * Tagastab Transactioni Itemi.
         * @return  Transaction Item.
         */
        public Item getItem() {
            return this.item;
        }

        /**
         * Tagastab Transactioni koguse.
         * @return  Transactioni kogus.
         */
        public int getQuantity() {
            return this.quantity;
        }

        /**
         * Tagastab Transactioni Stringi kujul.
         * @return Transaction Stringi kujul.
         */
        public String toString() {
            String s = "";
            System.out.println("Item: " + item.toString());
            System.out.println("Quantity: " + quantity);
            System.out.println("Order id: " + orderId);
            return s;
        }
    }

    /**
     * Tagastab Transactioni Stringi kujul.
     * @param t Transaction.
     * @return Transaction Stringi kujul.
     */
    public String transactionToString(Transaction t) {
        return t.toString();
    }


    /**
     * Tagastab Itemi kohta kõik Transactionid selles Stockis.
     * @param item Item, mille Transactioneid otsitakse.
     * @return  Kõik selle Itemi Transactionid.
     */
    public ArrayList<Transaction> getItemTransactions(Item item) {
        ArrayList<Transaction> itemTransactions = new ArrayList<Transaction>();
        for (Transaction t : transactions) {
            if (t.item.equals(item)) itemTransactions.add(t);
        }
        return  itemTransactions;
    }


    /**
     * Tagastab kõik Transactionid.
     * @return  Kõik Transactionid.
     */
    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }

    /**
     * Tagastab Stocki stringina.
     * @return Stock stringina.
     */
    @Override
    public String toString() {
        String s = "";
        for (Tuple t : stockList) {
            s += t.toString() + "\n";
        }
        if (stockList.size() == 0) s += "{EMPTY}";
        return s;
    }

}