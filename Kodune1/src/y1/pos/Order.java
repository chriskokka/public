/*
 * Order
 *
 * Version 0.1
 *
 * Date 08/02/14
 *
 * Copyright Chris Kokka
 */
package y1.pos;

import y1.wms.Item;
import y1.wms.Tuple;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Klass tellimuste jaoks.
 * @version 0.1 08/02/2014
 * @author Chris Kokka
 */
public class Order {

    /**
     * Tellimuse id.
     */
    private int id;

    /**
     * Salvestab kõik Order-id, mis loodud.
     */
    private static ArrayList<Order> orders = new ArrayList<Order>();

    /**
     * Toodete list.
     * @see ArrayList
     */
    private ArrayList<Tuple> products;

    /**
     * Iteraatori positsioon toodete listis.
     */
    private int position;

    /**
     * Unikaalsete id-de staatiline muutuja.
     */
    private static int counter = 0;

    /**
     * Märgib, kas Order on töödeldud või mitte.
     * Ehk siis, kas on Stocki saadetud.
     */
    private boolean processed;

    /**
     * Tellimuse konstruktor.
     */
    public Order() {
        this.id = counter++;
        this.products = new ArrayList<Tuple>();
        orders.add(this);
        this.processed = false;
    }

    /**
     * Teise Tellimuse põhjal loodava uue Tellimuse konstruktor.
     * @param o Tellimus, mille põhjal luuakse uus Tellimus.
     */
    public Order(Order o) {
        this.id = counter++;
        this.products = new ArrayList<Tuple>();
        Iterator<Tuple> iter = o.iterator();
        while (iter.hasNext())  {
          products.add(iter.next());
        }
        orders.add(this);
        this.processed = false;
    }

    /**
     * Tagastab id põhjal Tellimuse.
     * @param findId Tellimuse id.
     * @return Leitud Tellimus.
     */
    public static Order getOrder(int findId) {
        for (Order o : orders) {
            if (o.getId() == findId) return o;
        }
        return new Order();
    }

    /**
     * Lisab toote ja koguse tellimusele.
     * @param item Sisestatav toode.
     * @param quantity Toote kogus.
     */
    public boolean add(Item item, int quantity) {
        if (quantity <= 0) return false;
        Tuple t = new Tuple(item, quantity);
        products.add(t);
        return true;
    }

    /**
     * Tagastab tellimuse stringi kujul.
     * @return Tellimus stringi kujul.
     */
    @Override
    public String toString(){
        String s = "Order id: " + this.id + "\n";
        for (Tuple t : products) {
            Item item = t.getItem();
            s += "Toode: " + item.getName() + " " + String.format("%.2f", item.getPrice()) + " EUR  ";
            s += "Kogus: " + t.getQty() + "\n";
        }
        if (products.size() == 0) s+= "{EMPTY ORDER}";
        return s;
    }

    /**
     * Tagastab tellimuse id.
     * @return Tellimuse id.
     */
    public int getId(){
        return this.id;
    }

    /**
     * Setib muutuja processed true-ks.
     * Kasutatakse siis, kui Order jõuab Stocki.
     */
    protected void setProcessed(Order o) {
        o.processed = true;
    }

    /**
     * Tagastab Orderi processed väärtuse.
     * @return Processed väärtus.
     */
    protected boolean getProcessed(Order o) {
        return o.processed;
    }

    /**
     * Tagastab orderIteratori.
     * Kasutatakse tellimuse alla kuuluvate toodete üle itereerimiseks.
     * @return Iteraator tellimuse toodete ja koguste jaoks.
     */
    public OrderIterator iterator() {
        return new OrderIterator();
    }

    /**
     * Privaatklass pakkumaks võimalust Tellimuse alla kuuluvaid
     * tooteid ja nende koguseid itereerida.
     */
    private class OrderIterator implements Iterator<Tuple> {

        /**
         * OrderIteratori konstruktor.
         */
        public OrderIterator() {
            position = 0;
        }

        /**
         * Kontrollib, kas on järgmist elementi.
         * @return Kas on järgmine element olemas.
         */
        @Override
        public boolean hasNext() {
            return (position < products.size());
        }

        /**
         * Tagastab järgmise elemendi.
         * @return Järgmine element.
         */
        @Override
        public Tuple next() {
            Tuple t = products.get(position);
            position++;
            return t;
        }

        /**
         * Eemaldab praegusel positsioonil oleva elemendi.
         */
        @Override
        public void remove() {
            products.remove(position);
        }

    } // End OrderIterator class
} // End Order class
