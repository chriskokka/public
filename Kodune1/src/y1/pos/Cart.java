/*
 * Cart
 *
 * Version 0.1
 *
 * Date 08/02/14
 *
 * Copyright Chris Kokka
 */

package y1.pos;

import y1.wms.Item;
import y1.wms.Stock;
import y1.wms.Tuple;
import java.util.ArrayList;

/**
 * Klass poe ostukorvi jaoks.
 * @version 0.1 08/02/14
 * @author Chris Kokka
 */
public class Cart extends Stock {

    /**
     * Klassi Cart konstruktor.
     */
    public Cart() {
        this.stockList = new ArrayList<Tuple>();
    }

    /**
     * Lisab ostukorvi toote.
     * @return Kas õnnestus.
     */
    public boolean add(Item item) {
        Order o = new Order();
        o.add(item, 1);
        return process(o, "receive");
    }

    /**
     * Lisab ostukorvi rohkem kui 1 toodet.
     * @return Kas õnnestus.
     */
    public boolean add(Item item, int quantity) {
        if (quantity <= 0) return false;
        Order o = new Order();
        o.add(item, quantity);
        return process(o, "receive");
    }

    /**
     * Muudab toote kogust.
     * @return Kas õnnestus.
     */
    public boolean change(Item item, int quantity) {
        if (quantity <= 0) return false;
        Order o = new Order();
        o.add(item, quantity);
        return process(o, "change");
    }

    /**
     * Eemaldab toote ostukorvist.
     * @return  Kas õnnestus.
     */
    public boolean remove(Item item) {
        Order o = new Order();
        o.add(item, 9999);
        return process(o, "remove");
    }

    /**
     * Tagastab Carti kogumaksumuse.
     * @return Cart kogumaksumus.
     */
    public String getTotal() {
        double total = 0.00;
        for (Tuple t : stockList) {
            total += t.getItem().getPrice() * t.getQty();
        }
        return String.format("%.2f", total);
    }

    /**
     * Tagastab Cart-i põhjal koostatud Tellimuse.
     * @return  Koostatud Tellimus.
     */
    public Order checkOut() {
        Order o = new Order();
        for (Tuple t : stockList) {
            o.add(t.getItem(), t.getQty());
        }
        this.clear();
        return o;
    }

    /**
     * Tühjendab ostukorvi.
     */
    public String clear() {
        Order o = new Order();
        for (Tuple t : stockList) {
            o.add(t.getItem(), t.getQty());
        }
        boolean result = process(o, "remove");
        return result ? "Cart-i tühjendamine õnnestus." : "Cart-i tühjendamine ebaõnnestus.";
    }

}
